pub fn roman_to_int(s: String) -> i32 {
    let mut iter = s.chars();
    let mut main_char:char=' ';
    if let Some(chr) = iter.next(){
        main_char = chr;
       
    };
    let mut second_char = match iter.next(){
        Some(chr)=>chr,
        None=> {return eval_single(main_char);}
    };

  
    let mut result = 0;
    loop{
        let tmp = eval_double(main_char, second_char);
        if tmp != -1{
            result +=tmp;
            main_char = match iter.next(){
                Some(chr)=>chr,
                None=> {return result;}
            };
            second_char = match iter.next(){
                Some(chr)=>chr,
                None=> {
                    result += eval_single(main_char);
                    return result;
                }
            };
       
        }else{
            result += eval_single(main_char);
            main_char = second_char;
            second_char = match iter.next(){
                Some(chr)=>chr,
                None=> {
                    result += eval_single(main_char);
                    return result;
                }
            };
        }
    }
}

fn eval_double(main_char:char, second_char:char)->i32{
    if main_char == 'I' {
        if second_char == 'V' {
            return 4;
        }else if second_char == 'X'{
            return 9;
        }else{
            -1
        }
    }else if main_char == 'X'{
        if second_char == 'L' {
            return 40;
        }else if second_char == 'C'{
            return 90;
        }else{
            -1
        }

    }else if main_char == 'C'{
        if second_char == 'D' {
            return 400;
        }else if second_char == 'M'{
            return 900;
        }else{
            -1
        }
    }else{
        -1
    }
}

fn eval_single(chr:char) ->i32{
    match chr{
        'I'=>1,
        'V'=>5,
        'X'=>10,
        'L'=>50,
        'C'=>100,
        'D'=>500,
        'M'=>1000,
        _other=>-1
    }
}