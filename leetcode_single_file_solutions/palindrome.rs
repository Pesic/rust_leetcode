
pub fn is_palindrome(x: i32) -> bool {
    let mut curr_x = x;
    if curr_x < 0 {
        return false;
    }
    let mut digits = Vec::with_capacity(32);
    loop {
        let digit = curr_x % 10;
        let divisor = curr_x / 10;
        curr_x=divisor;
        digits.push(digit);
        if divisor == 0 {
            break;
        }
    }
    let mut new_x = 0;
    let mut cnt = 1;
    for i in digits.iter().rev() {
        if cnt == 1{
            new_x += i;
        }else{
            new_x +=i*cnt;
        }
        cnt*=10;
    }
    if new_x == x {
        true
    }else {
        false
    }
}