pub fn is_valid(s: String) -> bool {
    let mut stack:Vec<char> = Vec::new();
    for chr in s.chars() {
        match chr {
            '(' => {stack.push(chr);},
            '{' => {stack.push(chr);},
            '[' => {stack.push(chr);},
            ')' => {
            	if stack.len()==0 {
            		return false;
            	}
            	check(')', &mut stack);},
            ']' => {
            	if stack.len()==0 {
            		return false;
            	}
            	check(']', &mut stack);},
            '}' => {
            	if stack.len()==0 {
            		return false;
            	}
            	check('}', &mut stack);},
            _other => {}
        }
    }
    stack.len() == 0
}

pub fn invert(chr: char)-> char {
    match chr{
        '('=>')',
        '{'=>'}',
        '['=>']',
        ')'=>'(',
        '}'=>'{',
        ']'=>'[',
        _other =>' '
    }
}

pub fn check(chr: char, stack: & mut Vec<char>){
    if invert(chr) == stack[stack.len()-1] {
        stack.pop();
    }else{
    	stack.push(chr);
    }
}