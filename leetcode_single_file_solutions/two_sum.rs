use std::cmp::Ordering;

#[derive(Debug)]
struct Number{
    pub num:i32,
    pub ind:i32,
}

pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
    let mut numbers:Vec<Number> = Vec::new();
    let mut index = 0;
    for num in nums{
        numbers.push( Number{num:num, ind:index} ) ;
        index = index+1;

    }
    println!("{:?}", numbers );
    numbers.sort_by(|num1, num2|{
        if num1.num < num2.num{
            Ordering::Less
        }else if num1.num == num2.num{
            Ordering::Equal
        }else {
            Ordering::Greater
        }
    });
    println!("{:?}", numbers );

    let mut iter_front = numbers.iter();
    let mut iter_back = numbers.iter().rev();

    let mut num_front = iter_front.next().unwrap();
    let mut num_back = iter_back.next().unwrap();

    loop{
        
        let sum = num_front.num + num_back.num;
        if sum == target {
            return vec![num_front.ind, num_back.ind];
        }else if sum > target {
            num_back = iter_back.next().unwrap();
        }else {
            num_front = iter_front.next().unwrap();
        }
    }

    
}