
use std::cmp::Ordering;
pub fn longest_common_prefix(strs: Vec<String>) -> String {
    let mut strings = strs;
    if strings.len() == 0{
        return String::new();
    }
    strings.sort_by(
        |s1, s2|{
            if s1.len()<s2.len(){
                Ordering::Less
            }else if s1.len()>s2.len(){
                Ordering::Greater
            }else{
                Ordering::Equal
            }

        }
        );
    
    // let max_len = vec[0].len();
    let mut iter = strings.iter();
    let mut shortest = iter.next().unwrap().to_string();
    loop{
        let mut found = true;
        while let Some(str) = iter.next() {
            if !str.starts_with(&shortest){
                found =false;
                break;
            }
        }
        if !found{
            match shortest.pop(){
                Some(_chr)=>{
                    iter = strings.iter();
                },
                None=>{return String::new();}
            }
        }else{
            return shortest.to_string();
        }
    }

}