use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Bad input");
    let num_str: String = input.trim().to_string();
    let mut vec: Vec<i32> = Vec::new();
    for elem in num_str.split(" ") {
        vec.push(elem.parse().expect("Bad number format"));
    }
    let k = remove_duplicates(&mut vec);
    println!("k: {}", k);
    println!("{:?}", vec);
}

pub fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
    let mut dest_ind = 0;
    let mut source_ind = 0;
    if nums.len() == 0 {
        return 0;
    }
    if nums.len() == 1 {
        return 1;
    }
    loop {
        dest_ind += 1;
        let src_num = nums.get(source_ind).unwrap();
        match nums.get(dest_ind) {
            None => {
                source_ind += 1;
                return source_ind.try_into().unwrap();
            }
            Some(dst_num) => {
                source_ind += 1;
                if dst_num != src_num {
                    dest_ind = source_ind;
                } else {
                    let to_insert;
                    loop {
                        match nums.get(dest_ind) {
                            None => {
                                return source_ind.try_into().unwrap();
                            }
                            Some(dst_num) => {
                                if dst_num != src_num {
                                    to_insert = *dst_num;
                                    break;
                                }
                                dest_ind += 1;
                            }
                        }
                    }
                    nums.insert(source_ind, to_insert);
                }
            }
        }
    }
}
