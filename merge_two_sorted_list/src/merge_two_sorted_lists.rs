// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

pub struct Solution;

impl Solution {
    pub fn revert_list(l: Option<Box<ListNode>>) -> Vec<i32> {
        let mut l = l;
        let mut res_vec: Vec<i32> = Vec::new();
        loop {
            match l {
                Some(elem) => {
                    res_vec.insert(0, (*elem).val);
                    l = (*elem).next;
                }
                None => {
                    break;
                }
            }
        }
        res_vec
    }

    pub fn list_from_vec(v: Vec<i32>) -> Option<Box<ListNode>> {
        let mut head: Box<ListNode> = Box::new(ListNode { val: 0, next: None });
        let mut iter = v.iter().rev();
        let mut is_head = true;
        loop {
            let value = iter.next();
            if let Some(val) = value {
                if is_head {
                    head = Box::new(ListNode {
                        val: *val,
                        next: None,
                    });
                    is_head = false;
                } else {
                    let elem = Box::new(ListNode {
                        val: *val,
                        next: Some(head),
                    });
                    head = elem.clone();
                }
            } else {
                break;
            }
        }
        Some(head)
    }

    pub fn merge_two_lists(
        l1: Option<Box<ListNode>>,
        l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        if let None = l1 {
            if let None = l2 {
                return None;
            }
            return l2;
        } else if let None = l2 {
            if let None = l1 {
                return None;
            }
            return l1;
        } else {
            let result = Solution::merge_two_lists_internal(l1, l2);
            let vec = Solution::revert_list(result);
            let result = Solution::list_from_vec(vec);
            return result;
        }
    }

    fn merge_two_lists_internal(
        l1: Option<Box<ListNode>>,
        l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut result_head: Box<ListNode> = Box::new(ListNode { val: 0, next: None });
        let head1 = l1.clone();
        let mut head1 = head1.unwrap();
        let head2 = l2.clone();
        let mut head2 = head2.unwrap();
        let mut head_init = false;
        loop {
            if (*head1).val <= (*head2).val {
                if !head_init {
                    result_head = Box::new(ListNode {
                        val: head1.val,
                        next: None,
                    });
                    head_init = true;
                    println!("1 {:?}", result_head);
                } else {
                    let new_elem = Box::new(ListNode {
                        val: (*head1).val,
                        next: Some(result_head),
                    });
                    result_head = new_elem;
                    println!("2 {:?}", result_head);
                }
                let next_head1 = head1.clone().next;
                match next_head1 {
                    None => loop {
                        let new_elem = Box::new(ListNode {
                            val: (*head2).val,
                            next: Some(result_head),
                        });
                        result_head = new_elem;
                        println!("3 {:?}", result_head);
                        let next_head2 = head2.clone().next;
                        match next_head2 {
                            None => {
                                return Some(result_head);
                            }
                            Some(next) => {
                                head2 = next;
                            }
                        }
                    },
                    Some(elem) => {
                        head1 = elem;
                    }
                }
            } else {
                if !head_init {
                    result_head = Box::new(ListNode {
                        val: head2.val,
                        next: None,
                    });
                    head_init = true;
                    println!("4 {:?}", result_head);
                } else {
                    let new_elem = Box::new(ListNode {
                        val: (*head2).val,
                        next: Some(result_head),
                    });
                    result_head = new_elem;
                    println!("5 {:?}", result_head);
                }
                let next_head2 = head2.clone().next;
                match next_head2 {
                    None => loop {
                        let new_elem = Box::new(ListNode {
                            val: (*head1).val,
                            next: Some(result_head),
                        });
                        result_head = new_elem;
                        let next_head1 = head1.clone().next;
                        println!("6 {:?}", result_head);
                        match next_head1 {
                            None => {
                                return Some(result_head);
                            }
                            Some(next) => {
                                head1 = next;
                            }
                        }
                    },
                    Some(elem) => {
                        head2 = elem;
                    }
                }
            }
        }
    }
}
