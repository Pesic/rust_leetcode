use std::io;
mod merge_two_sorted_lists;
use merge_two_sorted_lists::{ListNode, Solution};

enum State {
    INIT,
    HEAD,
    MID,
    TAIL,
}

fn read_input_list() -> String {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Bad input");
    input.trim().parse().expect("Bad input")
}

fn create_list(list_str: String) -> Option<Box<ListNode>> {
    if list_str == String::from("[]") {
        return None;
    }
    let mut head: Box<ListNode> = Box::new(ListNode { val: 0, next: None });
    let mut state = State::INIT;
    let mut iter = list_str.chars().rev();
    let mut number = String::new();
    loop {
        if let State::TAIL = state {
            break;
        }
        let chr = iter.next();
        if let Some(c) = chr {
            if c == ']' {
                state = State::HEAD;
                continue;
            }
            if c == ',' {
                if let State::HEAD = state {
                    state = State::MID;
                }
                continue;
            }
            if c == '[' {
                state = State::TAIL;
                continue;
            }
            if c == '-' {
                number.push('-');
                continue;
            }
            number.push(c);
            let val: i32 = String::from(number.clone())
                .parse()
                .expect("Bad list element");
            number.clear();

            if let State::HEAD = state {
                head = Box::new(ListNode {
                    val: val,
                    next: None,
                });
            } else {
                let elem = Box::new(ListNode {
                    val: val,
                    next: Some(head),
                });
                head = elem.clone();
            }
        }
    }
    Some(head)
}

fn main() {
    let str1 = read_input_list();
    let str2 = read_input_list();
    let l1 = create_list(str1);
    let l2 = create_list(str2);

    let res = Solution::merge_two_lists(l1, l2);
    println!("{:?}", res);
}
#[cfg(test)]
mod tests {
    use super::*;

    fn check_merge(l1_str: String, l2_str: String, l_ref_str: String) {
        let l1 = create_list(l1_str);
        let l2 = create_list(l2_str);
        let mut l_ref = create_list(l_ref_str);
        let mut l_result = Solution::merge_two_lists(l1, l2);

        loop {
            if let None = l_result {
                break;
            }
            let ref_result = l_ref.unwrap();
            let result = l_result.unwrap();
            assert_eq!(result.val, ref_result.val);
            l_ref = ref_result.next;
            l_result = result.next;
        }
        if let Some(_) = l_ref {
            panic!("result len longer match expected result len");
        }
    }

    #[test]
    fn test_list_merge() {
        check_merge(
            String::from("[1]"),
            String::from("[1]"),
            String::from("[1,1]"),
        );
    }
    #[test]
    fn test_list_merge1() {
        check_merge(
            String::from("[1]"),
            String::from("[2]"),
            String::from("[1,2]"),
        );
    }
    #[test]
    fn test_list_merge2() {
        check_merge(
            String::from("[2]"),
            String::from("[1]"),
            String::from("[1,2]"),
        );
    }
    #[test]
    fn test_list_merge3() {
        check_merge(
            String::from("[1,2]"),
            String::from("[1]"),
            String::from("[1,1,2]"),
        );
    }
    #[test]
    fn test_list_merge4() {
        check_merge(
            String::from("[1,2]"),
            String::from("[2]"),
            String::from("[1,2,2]"),
        );
    }
    #[test]
    fn test_list_merge5() {
        check_merge(
            String::from("[1,2]"),
            String::from("[3]"),
            String::from("[1,2,3]"),
        );
    }
    #[test]
    fn test_list_merge6() {
        check_merge(
            String::from("[2,3]"),
            String::from("[1]"),
            String::from("[1,2,3]"),
        );
    }
    #[test]
    fn test_list_merge7() {
        check_merge(
            String::from("[1,3]"),
            String::from("[2]"),
            String::from("[1,2,3]"),
        );
    }

    #[test]
    fn test_list_merge8() {
        check_merge(
            String::from("[1,3,5,6]"),
            String::from("[2,4,8,10]"),
            String::from("[1,2,3,4,5,6,8,10]"),
        );
    }
    #[test]
    fn test_list_merge9() {
        check_merge(String::from("[]"), String::from("[]"), String::from("[]"));
    }
    #[test]
    fn test_list_merge10() {
        check_merge(String::from("[0]"), String::from("[]"), String::from("[0]"));
    }
    #[test]
    fn test_list_merge11() {
        check_merge(String::from("[]"), String::from("[0]"), String::from("[0]"));
    }
    #[test]
    fn test_list_merge12() {
        check_merge(
            String::from("[-10,-6,-6,-6,-3,5]"),
            String::from("[]"),
            String::from("[-10,-6,-6,-6,-3,5]"),
        );
    }

    #[test]
    fn test_list_merge13() {
        check_merge(
            String::from("[]"),
            String::from("[-10,-6,-6,-6,-3,5]"),
            String::from("[-10,-6,-6,-6,-3,5]"),
        );
    }
}
